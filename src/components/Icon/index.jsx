import PropTypes from 'prop-types';

import './styles.scss';

export const Icon = ({ srcImg, altText, keyProp, className = '' }) => (
  <button key={keyProp}>
    <img src={srcImg} alt={altText} className={className} />
  </button>
);

Icon.propTypes = {
  srcImg: PropTypes.string.isRequired,
  altText: PropTypes.string.isRequired,
  keyProp: PropTypes.number.isRequired,
  className: PropTypes.string,
};
