import logoData from './dataLogo';
import dataFooterLinks from './dataFooterLinks';
import { Icon } from '../Icon';
import { Link } from '../Link';

import './styles.scss';

export const BottomSection = () => {
  const RenderText = () => (
    <div className="title">
      <h2>Keep in touch with us</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam odit corporis soluta! In, incidunt eum labore
        harum exercitationem sed natus perspiciatis? Nesciunt, dicta sint? Voluptas rem molestias quo dolor numquam.
      </p>
    </div>
  );

  const RenderForm = () => (
    <form className="formEmail">
      <input type="email" className="inputEmail" placeholder="Enter your email to update" />
      <button className="submit" type="submit">
        Submit
      </button>
    </form>
  );

  const RenderSocialLogoFooter = () => (
    <div className="socialLogoFooter">
      {logoData.map(({ srcImg, altText }, index) => (
        <Icon key={index} keyProp={index} srcImg={srcImg} altText={altText} />
      ))}
    </div>
  );

  const RenderFooter = () => (
    <footer>
      <div className="addressFooter">
        <small>HALOVIETNAM LTD 66, Dang Van ngu, Dong Da Hanoi, Vietnam contact@halovietnam.com +844 35149182</small>
      </div>
      <div className="footerColumns">
        <div className="firstColumn">
          {dataFooterLinks.firstColumn.map(({ name, link }, index) => (
            <Link key={index} link={link} className="footerLink">
              {name}
            </Link>
          ))}
        </div>
        <div className="secondColumn">
          {dataFooterLinks.secondColumn.map(({ name, link }, index) => (
            <Link key={index} link={link} className="footerLink">
              {name}
            </Link>
          ))}
        </div>
        <div className="thirdColumn">
          {dataFooterLinks.thirdColumn.map(({ name, link }, index) => (
            <Link key={index} link={link} className="footerLink">
              {name}
            </Link>
          ))}
        </div>
        <div className="fourthColumn">
          {dataFooterLinks.fourthColumn.map(({ name, link }, index) => (
            <Link key={index} link={link} className="footerLink">
              {name}
            </Link>
          ))}
        </div>
      </div>
    </footer>
  );

  return (
    <section id="bottomSection">
      <RenderText />
      <RenderForm />
      <RenderSocialLogoFooter />
      <RenderFooter />
    </section>
  );
};
