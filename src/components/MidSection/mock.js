import featureIcon1 from '../../assets/featureIcon1.png';
import featureIcon2 from '../../assets/featureIcon2.png';
import featureIcon3 from '../../assets/featureIcon3.png';
import featureIcon4 from '../../assets/featureIcon4.png';
import featureIcon5 from '../../assets/featureIcon5.png';
import featureIcon6 from '../../assets/featureIcon6.png';

export default [
  {
    image: featureIcon1,
    title: 'Fully Responsive',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
  {
    image: featureIcon2,
    title: 'HTML3 & CSS3',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
  {
    image: featureIcon3,
    title: 'Fully Layered PSD',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
  {
    image: featureIcon4,
    title: 'Email Template',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
  {
    image: featureIcon5,
    title: 'Font Awesome Icons',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
  {
    image: featureIcon6,
    title: 'Free to Download',
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore, et! Odit, quis voluptatem consequuntur laboriosam recusandae illum rerum labore quidem nam quod error saepe vel.',
  },
];
