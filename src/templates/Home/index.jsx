import { BottomSection } from '../../components/BottomSection';
import { MidSection } from '../../components/MidSection';
import { TopSection } from '../../components/TopSection';

const Home = () => {
  return (
    <div className="content">
      <TopSection />
      <MidSection />
      <BottomSection />
    </div>
  );
};

export default Home;
