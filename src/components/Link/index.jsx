import PropTypes from 'prop-types';
import './styles.scss';

export const Link = ({ link, className, children = null, onClick = undefined }) => (
  <a href={link} className={className} onClick={onClick}>
    {children}
  </a>
);

Link.propTypes = {
  link: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  children: PropTypes.node,
  onClick: PropTypes.func,
};
