import facebookIcon from '../../assets/facebookIcon.png';
import twitterIcon from '../../assets/twitterIcon.png';
import googleIcon from '../../assets/googleIcon.png';
import pinterestIcon from '../../assets/pinterestIcon.png';

export default [
  {
    srcImg: facebookIcon,
    altText: 'Ícone do Facebook',
  },
  {
    srcImg: twitterIcon,
    altText: 'Ícone do Twitter',
  },
  {
    srcImg: googleIcon,
    altText: 'Ícone do Google',
  },
  {
    srcImg: pinterestIcon,
    altText: 'Ícone do Pinterest',
  },
];
