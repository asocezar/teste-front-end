export default {
  firstColumn: [
    {
      name: 'Examples',
      link: '#',
    },
    {
      name: 'Shop',
      link: '#',
    },
    {
      name: 'License',
      link: '#',
    },
  ],
  secondColumn: [
    {
      name: 'Contact',
      link: '#',
    },
    {
      name: 'About',
      link: '#',
    },
    {
      name: 'Privacy Terms',
      link: '#',
    },
  ],
  thirdColumn: [
    {
      name: 'Download',
      link: '#',
    },
    {
      name: 'Support',
      link: '#',
    },
    {
      name: 'Documents',
      link: '#',
    },
  ],
  fourthColumn: [
    {
      name: 'Media',
      link: '#',
    },
    {
      name: 'Blog',
      link: '#',
    },
    {
      name: 'Forums',
      link: '#',
    },
  ],
};
