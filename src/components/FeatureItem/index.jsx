import PropTypes from 'prop-types';
import './styles.scss';

export const FeatureItem = ({ image, title, text, keyProp }) => {
  return (
    <div className="featureItem" key={keyProp}>
      <img className="featureImage" src={image} />
      <div className="featureText">
        <h3>{title}</h3>
        <p>{text}</p>
      </div>
    </div>
  );
};

FeatureItem.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  keyProp: PropTypes.number.isRequired,
};
