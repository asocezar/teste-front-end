# Test Front-End para empresa 2B

------------

## Etapas
- A primeira etapa foi decidir quais tecnologias eu utilizaria para completar o teste e ter um resultado satisfatório, otimizando meu tempo e a aplicação.

- Depois de decidir pela utilização da lib React com o pré-processador SASS, dividi o template da página em três seções, que ficam mais claras quando olhamos para a cor do background dessas seções: a primeira amarela, a segunda branca e a terceira preta.

- Após dividir as seções me atentei para os pequenos componentes que estariam inseridos nessas seções, principalmente para aspectos como: 
    - "O que está nesse projeto como hard-coded mas que em algum momento pode ser consumido de uma API ou CMS?"
    - "Quais são os componentes da página que possuem os mesmos atributos e podem ser reutilizados ao longo dela?"
    - "Como reduzir o render do componente para uma visualização mais fácil (dentro do código) do que está sendo renderizado por aquele componente?"
 
- Por fim, ao determinar esse pontos separei as informações que poderiam vir de fora da aplicação através de um CMS ou de uma API e fiz a programação direcionada para esses dados, que foram colocados na mesma pasta do componente que a utiliza como data*/.js.

- Os componentes reutilizados por mais de uma seção foram dividios em pastas separada dos das seções.

- Ao invés de retornar diretamente no return do componente que representa a section, foram divididas funções de render para facilitar a visualização do que cada seção vai retornar, sem que seja necessário muita busca nos arquivos da aplicação.

- Para começar a estilização, fiz a separação dentro de um arquivo .scss das cores mais utilizadas no site, como variáveis, para serem reutilizadas com mais facilidade, assim como tamanho das fonts e o padding e o tamanho máximo (*max-width*) para diferentes espaçamentos dependendo da media-query que também foi colocada em formato de variável.

- Por fim, utilizei o desenvolvimento em Mobile First para fazer o processo de responsividade da página. O principal ponto da responsividade está no 'menu'  que em tablets, laptops e dispositivos com largura acima de 1024px encontra-se em formato de um cabeçalho, enquanto em dispositivos com largura menor do que 767px têm o acesso ao menu através do ícone fixado no topo direito da página.
    - Para abertura e fechamento do menu, foi utilizado o sistema de adicionar classe ao elemento, sendo neste elemento o único que utilizei algum Hook (useRef) para facilitar o processo de referenciamento do elemento ao qual seria adicionada a classe.
 
 - No carrosel decidi por fazer as etapas do carrosel de acordo com os arquivos que poderiam ser acessados por aquela sessão e foram colocados como arquivo dataCarouselContent.js, para evitar o aparecimento de mais ou menos etapas do que o necessário e para facilitar caso sejam colocadas novas informações na página para serem adicionadas ao carrosel, sendo o acesso ao design da página feito novamente, somente se necessário.

------------
## Maiores desafios
- Eu pessoalmente nunca havia desenvolvido utilizando arquivo photoshop para a leitura do template e foi a etapa do processo que mais me foi demorada. Após algumas pesquisas em sites e alguns vídeos sobre essa ferramenta utilizei o próprio photoshop para fazer a extração dos ícones e imagens do arquivo.

------------
## Como compilar e rodar o projeto
- Primeiramente, é necessário fazer um clone dessa fork para a máquina. Para isso, se possuir o git instalado, é possível utilizar o terminal com o comando `git clone https://asocezar@bitbucket.org/asocezar/teste-front-end.git `, caso não possua o git instalado, é possivel fazer o dowload dos arquivos do repositório no menu no formato de três pontos, acima, na opção "Download this repository"

- Depois de baixado, na pasta master (geralmente a pasta raíz) do projeto deve ser feita a instalação dos pacotes utilizados no projeto, de acordo com seu gerenciador de pacotes, utilize:
    - `npm install` (caso utilize npm)
    - `yarn install` (caso use yarn)
 
- Ao final da instalação dos pacotes, pode ser utilizado o comando `npm start` ou `yarn start` para rodar a aplicação e abrir em seu navegador padrão.

______
## Deploy

[Vercel](https://teste-front-end-eight.vercel.app)
________________
Obrigado!! 😁
