import appleIcon from '../../assets/appleIcon.png';
import androidIcon from '../../assets/androidIcon.png';
import windowsIcon from '../../assets/windowsIcon.png';
import iPad from '../../assets/iPad.png';

export default {
  topSection: [
    {
      text: {
        title: 'Inspire your inspiration',
        subtitle: 'Simple to use for your app, products showcase and your inspiration',
        description:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum similique quos perferendis fuga necessitatibus,velit optio modi! Ea dolorem rerum praesentium. Soluta, illum. Omnis dicta iusto id, quod adipisci expedita.',
      },
      icons: [
        {
          srcImg: appleIcon,
          altText: 'Ícone da Apple',
        },
        {
          srcImg: androidIcon,
          altText: 'Ícone do Android',
        },
        {
          srcImg: windowsIcon,
          altText: 'Ícone do Windows',
        },
      ],
      image: {
        srcImg: iPad,
        altText: 'Imagem de um iPad com o logo da Tinyone',
      },
    },
  ],
};
