import logo from '../../assets/logo.png';
import dataCarouselContent from './dataCarouselContent';
import dataMenuLinks from './dataMenuLinks';
import menuIcon from '../../assets/menu-icon.png';

import './styles.scss';

import { Icon } from '../Icon';
import { Link } from '../Link';
import { useRef } from 'react';

export const TopSection = () => {
  const { topSection } = dataCarouselContent;

  const menu = useRef();

  const RenderHeader = () => (
    <header>
      <button className="openMenu" onClick={() => menu.current.classList.toggle('active')}>
        <img src={menuIcon} alt="Ícone para abrir o menu" />
      </button>
      <div className="menuTop" ref={menu}>
        <div className="logo">
          <Link link="/" className="logoLink">
            <img src={logo} />
          </Link>
        </div>
        <nav className="headerNav">
          {dataMenuLinks.map(({ name, link }, index) => (
            <Link
              className="menuTopLink"
              key={index}
              link={link}
              onClick={() => menu.current.classList.remove('active')}
            >
              {name}
            </Link>
          ))}
        </nav>
      </div>
    </header>
  );

  const RenderText = () => (
    <div className="topSectionText">
      {topSection.map(({ text }, index) => (
        <div key={index}>
          <h1>{text.title}</h1>
          <h2>{text.subtitle}</h2>
          <p>{text.description}</p>
        </div>
      ))}
    </div>
  );

  const RenderOSIcons = () => (
    <div className="oSIconsContainer">
      {topSection.map(({ icons }) =>
        icons.map(({ srcImg, altText }, index) => (
          <Icon key={index} keyProp={index} srcImg={srcImg} altText={altText} className="oSIcon" />
        )),
      )}
    </div>
  );

  const RenderImage = () => (
    <div className="topSectionImage">
      {topSection.map(({ image }, index) => (
        <img key={index} src={image.srcImg} alt={image.altText} className="imageTopSection" />
      ))}
    </div>
  );

  const RenderCarouselStages = () => (
    <div className="carouselStages">
      {topSection.map((content, index) => (
        <div key={index} className="carouselStagesCircle" />
      ))}
    </div>
  );

  return (
    <section id="topSection">
      <RenderHeader />
      <div className="topSectionContent">
        <div className="textColumn">
          <RenderText />
          <RenderOSIcons />
        </div>
        <RenderImage />
      </div>
      <RenderCarouselStages />
    </section>
  );
};
