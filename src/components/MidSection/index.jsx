import mock from './mock';
import { FeatureItem } from '../FeatureItem';

import './styles.scss';

export const MidSection = () => {
  const RenderText = () => (
    <div className="textFeatures">
      <h2>Tinyone features</h2>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nam dolore quas molestiae beatae quod? Illo beatae,
        odio necessitatibus, id dolore, minus asperiores tempora atque similique non vero? Neque, repudiandae a!
      </p>
    </div>
  );

  const RenderGridFeatures = () => (
    <div className="gridFeatures">
      {mock.map(({ title, image, text }, index) => (
        <FeatureItem key={index} keyProp={index} image={image} title={title} text={text} />
      ))}
    </div>
  );

  return (
    <section id="midSection">
      <RenderText />
      <RenderGridFeatures />
    </section>
  );
};
